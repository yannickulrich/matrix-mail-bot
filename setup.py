import setuptools

setuptools.setup(
    name='matrixmailbot',
    version='0.0.1',
    license='GPLv3',
    author='Yannick Ulrich',
    author_email='yannick.ulrich@durham.ac.uk',
    description='A matrix mail forwarding bot',
    url='https://gitlab.com/yannickulrich/matrix-mail-bot',
    packages=setuptools.find_packages(),
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Operating System :: OS Independent',
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: Developers',
        'Topic :: Communications :: Chat',
        'Topic :: Communications :: Email'
    ],
    install_requires=[
        'aiofiles~=0.6.0',
        'matrix-nio~=0.19.0',
        'humanize~=4.2.1',
        'requests~=2.28.0',
        'strictyaml~=1.6.1'
    ],
    entry_points={
        'console_scripts': [
            'matrix-mail-bot = matrixmailbot.__main__:main'
        ]
    },
    package_data={'matrixmailbot': ['icon.png']},
    include_package_data=True,
    zip_safe=False,
    python_requires='>=3.6'
)

