import logging
import asyncio
from .matrix import EmailBot
from .config import Config


def main():
    logging.basicConfig(level=logging.DEBUG)
    loop = asyncio.new_event_loop()
    bot = EmailBot(loop, Config('config.yaml'))
    loop.run_until_complete(
        bot.login()
    )
    try:
        loop.create_task(bot.run())
        loop.run_forever()
    except Exception:
        loop.run_until_complete(bot.close())


if __name__ == "__main__":
    main()
