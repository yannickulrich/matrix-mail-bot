from strictyaml import *
import types
import tempfile

_schema = Map({
    "config": Map({
        "homeserver": Url(),
        "username": Str(),
        "password": Str(),
        "securitykey": Str(),
        Optional("access_token", ""): Str(),
        Optional("device_id", ""): Str(),
        Optional("device_name", "postman"): Str(),
        "rooms": Seq(
            Map({
                "name": Str(),
                Optional("room"): Str()
            })
        ),
        "accounts": Seq(
            Map({
                Optional("active", True): Bool(),
                "name": Str(),
                "host": Str(),
                "username": Str(),
                Optional("type", "PlainWatcher"): Str(),
                Optional("password"): Str(),
                Optional("token"): Str(),
                Optional("access_token"): Str(),
                Optional("refresh_token"): Str(),
                Optional("sid"): Str(),
                Optional("uid"): Str(),
                Optional("refreshtime", 30): Int()
            })
        )
    })
})


class Config(types.SimpleNamespace):
    def __init__(self, fn):
        with open(fn) as fp:
            super().__init__(**load(fp.read(), _schema).data['config'])

    def write(self, fn=""):
        if fn:
            fp = open(fn, 'w')
        else:
            fp = tempfile.NamedTemporaryFile(
                mode='w',
                suffix='.yaml', prefix='config-',
                dir='.', delete=False
            )
        fp.write(as_document({
            'config': self.__dict__
        }).as_yaml())
        fp.close()
        return fp.name
