import asyncio
import imaplib
import requests
import email
import email.policy
from datetime import datetime, timedelta
import logging
import time
try:
    import proton.api
    import proton.exceptions
except ImportError:
    pass

logger = logging.getLogger(__name__)


class myimap(imaplib.IMAP4_SSL):
    def _mesg(self, s, secs=None):
        logger.debug(s)


class ProtonLogger:
    def __init__(self):
        self.logger = logger

    def set_log_path(self, path):
        pass


if "proton" in globals():
    proton.api.CustomLogger = ProtonLogger


class ReAuthError(Exception):
    def __init__(self, reauthCounter, delta, reason=""):
        self.counter = reauthCounter
        self.delta = delta
        self.reason = reason

    def __str__(self):
        if self.reason:
            return (
                f"{self.counter} re-authentications failed (delta={self.delta})"
                f" reason: {self.reason}"
            )
        else:
            return (
                f"{self.counter} re-authentications failed (delta={self.delta})"
                f". No reason given"
            )


class Watcher:
    def __init__(self):
        self.seen = set()
        self.last_check = 0
        self.nunread = 0
        self.reauthCounter = 0
        self.lastReauth = datetime.now()

    def login(self):
        logger.info(f"Selecting INBOX (host {self.host})")
        try:
            rv, dat = self.M.select('INBOX')
        except TimeoutError:
            logger.error("Select failed, timeout")
            return
        except Exception as err:
            logger.error("Select failed")
            logger.error(err)
            return

        if rv != "OK":
            logger.error(f"Select failed rv={rv}")
            logger.error(dat)
            self.logout()
            return

        self.active = True

    def connect(self):
        imaplib.Debug = 4
        if ":" in self.host:
            h, p = self.host.split(":")
            self.M = myimap(h, p)
        else:
            self.M = myimap(self.host)

    def logout(self):
        self.active = False
        logger.info(f"Logging out (host {self.host})")
        try:
            self.M.logout()
        except Exception as err:
            logger.error("Logout failed")
            logger.error(err)

    def reauth(self):
        self.active = False
        self.reauthCounter += 1
        logger.warn(
            f"Re-authenticating {self.host} ({self.reauthCounter} try)"
        )
        delta = datetime.now() - self.lastReauth
        logger.warn(f"Last reauth: {self.lastReauth} ({delta} ago)")

        try:
            self.logout()
        except Exception as err:
            logger.warn(f"Couldn't logout {self.host}")
            logger.warn(err)

        if delta.total_seconds() < 600:
            logger.error(
                "Last re-authentication was less than 10min ago."
                " Something isn't okay"
            )
            raise ReAuthError(
                self.reauthCounter, delta, "Too recent"
            )

        try:
            self.login()
        except Exception as err:
            logger.error(f"Re-authentication failed {err}")
            raise ReAuthError(
                self.reauthCounter, delta,
                f"Login failed: {err}"
            )

        self.lastReauth = datetime.now()

        try:
            rv, dat = self.M.search(None, 'UnSeen', 'Not Deleted')
        except TimeoutError:
            logger.error("Refresh failed, timeout")
            raise ReAuthError(
                self.reauthCounter, self.lastReauth,
                f"Refresh failed"
            )
        except Exception as err:
            logger.error("Refresh failed")
            logger.error(err)
            raise ReAuthError(
                self.reauthCounter, self.lastReauth,
                f"Refresh failed: {err}"
            )

        if rv != "OK":
            logger.error(f"Refresh failed rv={rv}")
            logger.error(dat)
            raise ReAuthError(
                self.reauthCounter, self.lastReauth,
                f"Refresh failed: {dat}"
            )

        self.seen = set(dat[0].split())

    def reactivate(self):
        logger.warn(f"Reactivating {self.host}")
        self.reauthCounter = 0
        self.lastReauth = datetime.fromtimestamp(0)
        self.reauth()

    def get_message(self, uid, fmt='(RFC822)'):
        logger.info(f"Fetching message {uid} (host {self.host})")
        try:
            rv, dat = self.M.fetch(uid, fmt)
        except TimeoutError:
            logger.error("Fetch failed, timeout")
            self.reauth()
            return
        except Exception as err:
            logger.error("Fetch failed")
            logger.error(err)
            self.reauth()
            return
        if rv != "OK":
            logger.error(f"Fetch failed rv={rv}")
            logger.error(dat)
            self.reauth()
            return

        for i in dat:
            if type(i) == tuple:
                header, msg = i
                if uid in header:
                    return email.message_from_bytes(
                        msg, policy=email.policy.default
                    )

    async def refresh(self):
        if not self.active:
            return

        logger.info(f"Run refresh (host {self.host})")
        self.last_check = datetime.now()
        try:
            rv, dat = self.M.search(None, 'UnSeen', 'Not Deleted')
        except TimeoutError:
            logger.error("Refresh failed, timeout")
            self.reauth()
            return
        except Exception as err:
            logger.error("Refresh failed")
            logger.error(err)
            self.reauth()
            return

        if rv != "OK":
            logger.error(f"Refresh failed rv={rv}")
            logger.error(dat)
            self.reauth()
            return

        uids = set(dat[0].split())
        self.nunread = len(uids)
        uids = uids - self.seen
        logger.info(
            f"{self.nunread} unread, {len(uids)} new (host {self.host})"
        )
        for uid in uids:
            msg = self.get_message(uid, '(RFC822.HEADER)')
            if msg:
                self.seen.add(uid)
                getter = lambda self=self, uid=uid: self.get_message(uid)
                await self.cb(getter, msg)

    async def watch(self, rate):
        self.rate = rate
        logger.info(f"Starting watcher every {rate}s (host {self.host})")
        try:
            while self.active:
                try:
                    await self.refresh()
                except ReAuthError as err:
                    if hasattr(self, 'finalCB'):
                        await self.finalCB(err.__str__())
                    return

                await asyncio.sleep(rate)
        except KeyboardInterrupt:
            self.logout()


class PlainWatcher(Watcher):
    def login(self):
        logger.info(f"Authenticating to {self.host}")
        self.connect()
        try:
            rv, dat = self.M.login(self.user, self.passwd)
        except Exception as err:
            logger.error("Authentication failed")
            logger.error(err)
            return
        if rv != "OK":
            logger.error(f"Login failed rv={rv}")
            logger.error(dat)
            return
        super().login()

    def __init__(self, host, user, passwd):
        logger.info(f"New plain watcher for {host}")
        self.host = host
        self.user = user
        self.passwd = passwd
        self.login()
        super().__init__()

    def chpasswd(self, passwd):
        self.logout()
        self.passwd = passwd
        self.login()


class XOauth2Watcher(Watcher):
    # Adapted from https://github.com/2e0byo/email

    # These are the secrets from Thunderbird, you can adapt them if
    # you want
    ID = "08162f7c-0fd2-4200-a84a-f25a4db0b584"
    SECRET = "TxRBilcHdC6WGBee]fs?QR:SJ8nI[g82"
    SCOPES = (
        "offline_access",
        "https://outlook.office.com/IMAP.AccessAsUser.All",
        "https://outlook.office.com/SMTP.Send",
    )
    BASE_URL = "https://login.microsoftonline.com/common/oauth2/"
    TOKEN_URL = BASE_URL + "v2.0/token"
    DEVICE_URL = BASE_URL + "v2.0/authorize"

    def auth_token(self, token):
        logger.info("Fetching auth token")
        resp = requests.post(
            self.TOKEN_URL,
            data={
                "client_id": self.ID,
                'tenant': 'common',
                "client_secret": self.SECRET,
                "refresh_token": token,
                "grant_type": "refresh_token",
            },
        )
        if not resp.status_code == 200:
            logger.error(f"Unable to authenticate: {resp.text}")
            raise Exception("Unable authenticate: " + resp.text)
        ans = resp.json()
        self.expect_reauth = datetime.now() + timedelta(seconds=ans['expires_in'])
        logger.debug(f"Auth token is {ans['access_token']}")
        logger.debug(f"Expecting reauth at {self.expect_reauth}")
        return ans['access_token']

    def login(self):
        logger.info(f"XOAuth2 for {self.host}")
        self.connect()
        sasl = (
            f"user={self.user}\1auth=Bearer {self.auth_token(self.token)}\1\1"
        ).encode()
        logger.debug(f'SASL string is {sasl}')

        try:
            rv, dat = self.M.authenticate('XOAUTH2', lambda x: sasl)
        except Exception as err:
            logger.error("Authentication failed")
            logger.error(err)
            return
        if rv != "OK":
            logger.error(f"Login failed rv={rv}")
            logger.error(dat)
            return

        super().login()

    def __init__(self, host, user, token):
        logger.info(f"New XOauth2 watcher for {host}")
        self.host = host
        self.user = user
        self.token = token
        self.login()
        super().__init__()

    def chpasswd(self, token):
        try:
            self.logout()
            self.token = token
            self.login()
            return self.active
        except Exception:
            return False


class ProtonWatcher(Watcher):
    API_URL = 'https://mail.proton.me/api'
    CACHE_PATH = 'cache'
    host = 'proton.me'

    def login(self):
        raise NotImplementedError

    def connect(self):
        raise NotImplementedError

    def get_message(self, uid):
        raise NotImplementedError

    def logout(self):
        # Logging out means destroying our keys. We won't do that
        return

    def refresh_token(self):
        logger.warn("Refreshing token ProtonMail")
        self.session._session_data = {}
        self.session.authenticate(self.user, self.passwd)

        self.active = True
        self.lastReauth = datetime.now()

    def reauth(self):
        self.active = False
        self.reauthCounter += 1
        logger.warn(f"Re-authenticating ProtonMail ({self.reauthCounter} try)")
        delta = datetime.now() - self.lastReauth
        logger.warn(f"Last reauth: {self.lastReauth} ({delta} ago)")

        if delta.total_seconds() < 600:
            logger.error(
                "Last re-authentication was less than 10min ago."
                " Something isn't okay"
            )
            raise ReAuthError(
                self.reauthCounter, self.lastReauth, "Too recent"
            )

        try:
            self.session.refresh()
            self.active = True
        except proton.exceptions.ProtonError as err:
            if hasattr(err, 'message'):
                if err.message == 'Invalid refresh token':
                    logger.warn("Invalid refresh token")
                    self.refresh_token()
                    return

            self.active = False
            if hasattr(err, 'code'):
                reason = f"{err} (code={err.code})"
            else:
                reason = str(err)
            logger.error(f"Re-authentication failed {reason}")
            raise ReAuthError(
                self.reauthCounter, self.lastReauth,
                f"Login failed: {reason}"
            )

        self.lastReauth = datetime.now()

    def parse_address(self, addr):
        if type(addr) == list:
            return [self.parse_address(i) for i in addr]
        if 'Name' in addr and addr['Name']:
            return f"{addr['Name']} <{addr['Address']}>"
        else:
            return addr['Address']

    async def refresh(self):
        if not self.active:
            return

        logger.info(f"Run refresh (proton mail)")
        self.last_check = datetime.now()
        try:
            logger.debug("Calling endpoint /mail/v4/messages?Unread=1")
            messages = self.session.api_request(
                "/mail/v4/messages?Unread=1"
            )['Messages']
        except proton.exceptions.ProtonError as err:
            self.active = False
            logger.error(f"Got proton error {err} (code={err.code})")
            if err.code == 10013:
                self.reauth()
            return
        except KeyError:
            logger.error("Invalid response")
            self.active = False

        logger.info(f"{len(messages)} obtained")
        self.nunread = len(messages)
        for i in messages:
            if i['ID'] in self.seen:
                continue

            d = datetime.fromtimestamp(i['Time'])

            msg = email.message.EmailMessage()
            msg['Subject'] = i['Subject']
            msg['From'] = self.parse_address(i['Sender'])
            msg['To'] = ', '.join(self.parse_address(i['ToList']))
            msg['CC'] = ', '.join(self.parse_address(i['CCList']))
            msg['Date'] = d.strftime("%a, %d %b %Y %H:%M:%S")
            self.seen.add(i['ID'])

            getter = lambda self=self, uid=i['ID']: self.get_message(uid)
            await self.cb(getter, msg)

    def connect(self, sid, uid, access_token, refresh_token):
        self.session = proton.api.Session(
            api_url=self.API_URL,
            log_dir_path='',
            cache_dir_path=self.CACHE_PATH,
            appversion="Other",
            user_agent="None",
            tls_pinning=False,
        )
        requests.utils.add_dict_to_cookiejar(self.session.s.cookies, {
            'Session-Id': sid, 'Tag': 'default'
        })
        self.session._session_data = {
            'UID': uid,
            'AccessToken': access_token,
            'RefreshToken': refresh_token,
            'PasswordMode': 1,
            'Scope': ['mail']
        }
        self.session.enable_alternative_routing = False

        if self.session.UID is not None:
            self.session.s.headers['x-pm-uid'] = self.session.UID
            self.session.s.headers['Authorization'] = (
                "Bearer " + self.session.AccessToken
            )
        self.reauth()

    def __init__(self, user, passwd, sid, uid, access_token, refresh_token):
        self.user = user
        self.passwd = passwd

        self.seen = set()
        self.last_check = 0
        self.nunread = 0
        self.reauthCounter = 0
        self.lastReauth = datetime.fromtimestamp(0)

        self.connect(sid, uid, access_token, refresh_token)

    def chpasswd(self, token):
        return False


def build_watcher(account):
    if account['type'] == 'XOauth2':
        return XOauth2Watcher(
            account['host'], account['username'], account['token']
        )
    elif account['type'] == 'Plain':
        return PlainWatcher(
            account['host'], account['username'], account['password']
        )
    elif account['type'] == 'Proton':
        if "proton" in globals():
            return ProtonWatcher(
                account['username'], account['password'],
                account['sid'],
                account['uid'],
                account['access_token'],
                account['refresh_token']
            )
        else:
            raise NameError
    raise NameError


def get_O365_token(username):
    import secrets
    import base64
    import hashlib
    import urllib
    import http.server
    import subprocess

    redirect_uri = 'http://localhost:9000/'
    verifier = secrets.token_urlsafe(90)

    challenge = base64.urlsafe_b64encode(
        hashlib.sha256(verifier.encode()).digest()
    )[:-1]

    url = XOauth2Watcher.DEVICE_URL + "?" + urllib.parse.urlencode({
                'client_id': XOauth2Watcher.ID,
                'scope': ' '.join(XOauth2Watcher.SCOPES),
                'response_type': 'code',
                'redirect_uri': redirect_uri,
            }, quote_via=urllib.parse.quote)
    subprocess.Popen(["xdg-open", url]).wait()

    global code
    code = ""

    class RequestHandler(http.server.BaseHTTPRequestHandler):
        def do_GET(self):
            global code
            qs = urllib.parse.parse_qs(urllib.parse.urlparse(self.path).query)
            code = qs['code'][0]
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()

        def log_message(self, *args, **kwargs):
            pass

    httpd=http.server.HTTPServer(('', 9000), RequestHandler)
    httpd.handle_request()
    httpd.server_close()

    resp = requests.post(
        XOauth2Watcher.TOKEN_URL,
        data={
            'grant_type': 'authorization_code',
            'code': code,
            'client_secret': XOauth2Watcher.SECRET,
            'redirect_uri': redirect_uri,
            'client_id': XOauth2Watcher.ID,
        }
    )
    return resp.json()['refresh_token']

if __name__ == "__main__":
    import sys
    print(get_O365_token(sys.argv[1]))
