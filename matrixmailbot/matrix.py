import asyncio
import aiofiles
from nio import (
    AsyncClient,
    MatrixRoom,
    RoomMessageText,
    LoginResponse,
    UploadResponse,
    ProfileSetDisplayNameResponse,
    ProfileGetAvatarError,
    RoomCreateError
)
import humanize
import os
import logging
from .imap import build_watcher, ReAuthError

logger = logging.getLogger(__name__)


class Bot:
    def __init__(self):
        self.ready = False

    async def message_callback(self, room, event):
        logger.debug(
            f"Received message in room {room.display_name} "
            f"from {room.user_name(event.sender)}"
        )

        if room.user_name(event.sender) == self.config.device_name:
            logger.debug("Message is from us, skip..")
            return
        if hasattr(self, 'name'):
            if room.user_name(event.sender) == self.name:
                logger.debug("Message is from us, skip..")
                return

        logger.info(f"Message received in room {room.display_name}")
        logger.info(f"{room.user_name(event.sender)} | {event.body}")

        args = event.body.strip().split(" ")
        cmd = args[0]
        logger.info(f"Command is {cmd}, arguments: {' '.join(args[1:])}")
        if hasattr(self, cmd):
            f = getattr(self, cmd)
            await self.send(room.room_id, await f(*args[1:]))
            logger.info(f"Handled command {cmd}")
        else:
            logger.warn(f"Can't understand command {cmd}")
            await self.send(
                room.room_id,
                f"I don't understand `{cmd}`. "
                f"Run `help` for available commands"
            )

    async def help(self):
        return self.help_str

    async def set_profile(self):
        if hasattr(self, 'name'):
            logger.info(f"display_name -> {self.name}")
            resp = await self.client.set_displayname(self.name)
            if type(resp) != ProfileSetDisplayNameResponse:
                logger.error(f"Couldn't set display name:")
                logger.error(resp)

        if hasattr(self, 'avatar'):
            logger.info(f"set_avatar -> {self.avatar}")
            current_avatar = await self.client.get_avatar()
            if type(current_avatar) != ProfileGetAvatarError:
                logger.warn("Avatar already set, won't set again")
                logger.debug(f"Avatar is {current_avatar.avatar_url}")
                return

            # Upload the image to the homeserver
            avatar_stat = os.stat(self.avatar)
            async with aiofiles.open(self.avatar, "rb") as avatar:
                logger.info(
                    f"Uploading {self.avatar} ({avatar_stat.st_size} bytes)"
                )
                upload_response, _ = await self.client.upload(
                    avatar,
                    content_type="image/png",
                    filename=self.avatar,
                    filesize=avatar_stat.st_size
                )

            # Set the uploaded image as our avatar
            if type(upload_response) == UploadResponse:
                resp = await self.client.set_avatar(
                    upload_response.content_uri
                )
                if type(resp) != ProfileSetAvatarError:
                    logger.error("Setting avatar failed")
                    logger.error(resp)
            else:
                logger.error("Upload of avatar failed")
                logger.error(upload_response)

    async def login(self):
        if self.config.access_token == "":
            logger.info("Starting login procedure using password")
            self.client = AsyncClient(
                self.config.homeserver, self.config.username
            )
            resp = await self.client.login(
                self.config.password, device_name=self.config.device_name
            )
            if not isinstance(resp, LoginResponse):
                logger.error("Login using password failed")
                logger.error(resp)
            self.config.access_token = resp.access_token
            self.config.device_id = resp.device_id
        else:
            logger.info("Starting login procedure using token")
            self.client = AsyncClient(self.config.homeserver)
            self.client.access_token = self.config.access_token
            self.client.user_id = self.config.username
            self.client.device_id = self.config.device_id

        logger.info("Synchronising")
        await self.client.sync()
        await self.set_profile()
        logger.info("Setting online")
        await self.client.set_presence('online')
        self.client.add_event_callback(
            self.message_callback, RoomMessageText
        )

        for room in self.config.rooms:
            if 'room' not in room:
                logger.warn("No room for user {room['name']}, creating new")
                room['room'] = await self.create_room(room['name'])
                logger.warn(f"room for {room['name']} -> {room['room']}")

            await self.send(room['room'], 'Type `help` for available commands')
        logger.info("Ready")
        self.ready = True

    async def close(self):
        if self.ready:
            logger.info("Shutting down")
            await self.client.set_presence('offline')
            await self.client.close()

    async def create_room(self, user):
        resp = await self.client.room_create(
            federate=False,
            is_direct=True,
            invite=[user]
        )
        if isinstance(resp, RoomCreateError):
            logger.error("Room creation failed")
            logger.error(resp)
            raise ValueError
        else:
            return resp.room_id

    async def delete_room(self, room):
        await self.client.room_kick(room['room'], room['name'])
        await self.client.room_leave(room['room'])

    async def send(self, room, msg):
        logger.info(f"Sending message to room {room}: {msg}")
        return await self.client.room_send(
            room_id=room,
            message_type="m.room.message",
            content={"msgtype": "m.text", "body": msg}
        )


class EmailBot(Bot):
    help_str = "Available commands: help, status, refresh, show"
    name = "Mail Forwarding Bot"
    avatar = 'icon.png'

    def __init__(self, loop, config):
        self.ready = False

        self.config = config
        self.loop = loop

        self.getters = {}
        self.watchers = {}

    async def login_accounts(self):
        for account in self.config.accounts:
            if not account['active']:
                continue
            w = build_watcher(account)
            for room in self.config.rooms:
                await self.send(
                    room["room"],
                    f"Logged into account {account['name']}"
                )
            w.cb = self.email_cb
            w.finalCB = self.final_email_cb
            self.loop.create_task(w.watch(account['refreshtime']))
            self.watchers[account['name']] = w

    async def close(self):
        if self.ready:
            for w in self.watchers.values():
                w.logout()
            await super().close()

    async def cat(self, num):
        n = int(num.replace("#", ""))
        logger.info(f"Attempting to fetch message {n}")
        for (nn, g) in self.getters.values():
            if nn == n:
                logger.info("Found message, fetch")
                msg = g()
                try:
                    body = msg.get_body('plain').get_content()
                    logger.info("Fetch successful, returning plain body")
                except AttributeError:
                    logger.info("No plain body, trying HTML")

                    try:
                        import html2text
                        body = html2text.html2text(
                            msg.get_body('html').get_content()
                        )
                        logger.info("Fetch successful, converted HTML body")
                    except ImportError:
                        logger.warning("Can't import html2text")
                        body = "Can't get plain text and html2text is unavaible"
                    except AttributeError:
                        logger.warning("No HTML either..")
                        body = "Can't get plain nor HTML"
                return body

        return "Can'f find this message"

    async def reactivate(self, account=""):
        if not self.ready:
            logger.warn("Not ready yet, dropping event")
            return
        ans = ""
        for i in self.find_accounts(account):
            if self.watchers[i].active:
                ans += f"Watcher {i} is already active, skipping\n"
                continue

            try:
                self.watchers[i].reactivate()
                self.loop.create_task(
                    self.watchers[i].watch(self.watchers[i].rate)
                )
                ans += f"Reactivated watcher {i}\n"
            except ReAuthError as err:
                self.watchers.pop(i)
                ans = (
                    f"Watcher {i} failed and couldn't be reactivated ({err})"
                )
                logger.error(ans)
                return ans

        return ans + await self.status()

    async def final_email_cb(self, msg):
        logger.error(f"Something went wrong {msg}")
        for room in self.config.rooms:
            await self.send(
                room['room'],
                f"Watcher failed!!!\n{msg}"
            )

    async def email_cb(self, getter, msg):
        logger.info(f"Email callback triggered (Subject: {msg['Subject']})")

        try:
            ind, _ = self.getters[msg['Message-ID']]
        except KeyError:
            ind = len(self.getters) + 1
            self.getters[msg['Message-ID']] = (ind, getter)

        if not self.ready:
            logger.warn("Not ready yet, dropping event")
            return
        for room in self.config.rooms:
            await self.send(
                room['room'],
                f"From: {msg['From']}\nSubject: {msg['Subject']}\n"
                f"Message is #{ind}"
            )

    def find_accounts(self, account=""):
        if not account:
            return self.watchers.keys()
        try:
            account = int(account)
            return [list(self.watchers.keys())[account-1]]
        except IndexError:
            # Account can't be found, index is non-sense
            return []
        except ValueError:
            pass
        # Search by name
        if account in self.watchers:
            return [account]
        else:
            return []

    async def status(self):
        ans = f"Have {len(self.watchers)} accounts\n"
        for n, (name, w) in enumerate(self.watchers.items()):
            if w.nunread == 0:
                msg = "no unread emails"
            elif w.nunread == 1:
                msg = "one unread email"
            else:
                msg = f"{w.nunread} unread emails"
            ans += f" {n+1}. `{name}`: {msg} "
            ans += f"(last checked {humanize.naturaltime(w.last_check)}"
            if not w.active:
                ans += ", disabled"
            ans += ")\n"
        logger.debug(f"Handling status\n{ans}")
        return ans

    async def show(self, account=""):
        if not self.ready:
            logger.warn("Not ready yet, dropping event")
            return

        ans = ""
        for i in self.find_accounts(account):
            logger.info(f"Reset and refresh watcher {i}")
            self.watchers[i].seen = set()
            try:
                await self.watchers[i].refresh()
            except ReAuthError as err:
                self.watchers.pop(i)
                ans = f"Watcher {i} failed and couldn't be reactivated ({err})"
                logger.error(ans)
                return ans

            ans += f"Reset and refreshed account {i}"
        if ans:
            return ans
        else:
            ans = f"Couldn't find account {account}"
            logger.warn(ans)
            return ans

    async def save(self, fn=""):
        logger.info(f"Writing config to {fn}")
        fn = self.config.write(fn)
        return f"Wrote config to {fn}"

    async def refresh(self, account=""):
        if not self.ready:
            logger.warn("Not ready yet, dropping event")
            return
        for i in self.find_accounts(account):
            try:
                await self.watchers[i].refresh()
            except ReAuthError as err:
                self.watchers.pop(i)
                ans = (
                    f"Watcher {i} failed and couldn't be reactivated ({err})"
                )
                logger.error(ans)
                return ans

        return "Refresh done\n" + await self.status()

    async def passwd(self, account, passwd):
        if not self.ready:
            logger.warn("Not ready yet, dropping event")
            return
        a = self.find_accounts(account)
        if len(a) != 1:
            ans = f"This matched {len(a)} accounts"
            logger.error(ans)
            return ans

        w = self.watchers[a[0]]
        success = w.chpasswd(passwd)
        if success:
            self.loop.create_task(w.watch(w.rate))
            return "Success, config not updated"
        else:
            return "Failed"

    async def run(self):
        logger.info("Starting main loop")
        self.loop.create_task(self.login_accounts())
        await self.client.sync_forever(timeout=30000)
